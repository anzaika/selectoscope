[![Build Status](https://travis-ci.org/anzaika/selectoscope.svg?branch=master)](https://travis-ci.org/anzaika/selectoscope)
[![Dependency Status](https://gemnasium.com/anzaika/selectoscope.svg)](https://gemnasium.com/anzaika/selectoscope)

Selectoscope
================

A web application for people searching for positive selection in their genomic data.
