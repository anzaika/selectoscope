require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  # root to: 'admin/dashboard#index'

  mount Sidekiq::Web => '/sidekiq'
end
