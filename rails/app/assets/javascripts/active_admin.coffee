#= require active_admin/base
#= require d3.min
#= require tnt.tree
#= require msa.min
#= require biojs-io-newick.min
#= require_tree ./modules
#= require_tree ./general
