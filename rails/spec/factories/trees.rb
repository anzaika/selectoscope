FactoryGirl.define do
  factory :tree do
    name 'tree'
    newick "(A(B,C));"
  end
end
